#!/usr/bin/perl
# this script has to be run as root

use warnings;
use strict;
use POSIX;

my $max_brightness_file = '/sys/class/backlight/intel_backlight/max_brightness';
my $curr_brightness_file = '/sys/class/backlight/intel_backlight/brightness';
my $new_brightness_file;

my $max_brightness;
my $curr_brightness;
my $new_brightness;
my $step;

sub load_file {local(@ARGV, $/) = @_; <>}
sub update_file_contents {
    my $filename = shift;
    open(my $fh, '>', $filename);
    print $fh $new_brightness;
    close $fh;
}

$max_brightness = load_file($max_brightness_file); chomp($max_brightness);
$curr_brightness = load_file($curr_brightness_file); chomp($curr_brightness);

# make steps for brightness
$step = $max_brightness / 20;
$step = ceil($step);
print "step = $step\n";

if (defined $ARGV[0] && $ARGV[0] eq "up"){
    $new_brightness = ($curr_brightness + $step);
    print "increasing\nnew brightness = " . $new_brightness . "\n";
}
else {
    $new_brightness = ($curr_brightness - $step);
    print "decreasing\nnew brightness = " . $new_brightness . "\n";
}
    
# print "max brightness = $max_brightness, step = $step";
&update_file_contents('/sys/class/backlight/intel_backlight/brightness');
