#!/home/riki/bin/bin/guile -s
!#
;; this script has to be run as root

(use-modules (ice-9 rdelim))

(define max-brightness-file "/sys/class/backlight/intel_backlight/max_brightness")
(define curr-brightness-file "/sys/class/backlight/intel_backlight/brightness")

(define (get-file-contents filename)
  (let* ((port (open-input-file filename))
	 (text (read-line port)))
    (close-port port)
    text))

(define (update-file-contents filename text-to-put)
  (let ((port (open-output-file filename)))
    (display text-to-put port)
    (close-port port)))

(define (get-step max-number split-by)
  (ceiling (/ max-number split-by)))

(let* ((option
	(if (eq? (length (command-line)) 1)
	    #f
	    (cadr (command-line))
	    ))
       (max-brightness (string->number (get-file-contents max-brightness-file)))
       (curr-brightness (string->number (get-file-contents curr-brightness-file)))
       (step (get-step max-brightness 20))
       (new-brightness
	(cond 
	 ((equal? option "up") (+ curr-brightness step))
	 ((equal? option "down") (- curr-brightness step))
	 (else (- curr-brightness step)))))
  (update-file-contents curr-brightness-file new-brightness))
