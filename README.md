# control backlight scripts
Scripts to control your laptop's LCD screen's backlight.  
Tested on a lenovo T430s on linux 4.9. Debian 9.


# Motivation
I don't use a desktop environment, but a pure window manager (sawfish). I use these scripts with a keybind for the original lenovo keybinds (fn + f8, fn + f9).

# Setup
There are two scripts.
You need to run them with the root user, or with sudo.  
The perl one can be used as it is in most systems I guess. But the scheme (guile) one needs to have its scheme path changed.

## Variables
The "step" variable defines how many light steps the script will have. It's 20 here (hardcoded). You're free to change the value.

# Usage
This is what I have in my sawfish conf:

```
(custom-set-keymap (quote global-keymap) (quote (keymap
  ((run-shell-command "sudo /home/riki/bin/bin/control_backlight.scm up") . "XF86MonBrightnessUp")
  ((run-shell-command "sudo /home/riki/bin/bin/control_backlight.scm down") . "XF86MonBrightnessDown")
  ...
  )))
```

